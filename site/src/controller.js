const mongoose = require('mongoose');
require('./model.js');
const modelo =  mongoose.model('dados');


exports.get =  async (req, res, next) => {
    try {  
        // const { page = 1 } = req.query
        const dados = await modelo.find() //paginate({}, { page, limit: 10 })
        
        // const {docs, limit, total, pages} = dados
        return res.render('index.ejs', {
            dados: dados 
        })

    } catch (err) {
        next(err);
    }
}