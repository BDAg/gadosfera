'use strict'

const mongoose = require('mongoose');

mongoose.set('useCreateIndex', true);
mongoose.set('useNewUrlParser', true);
mongoose.set('useUnifiedTopology', true);

var chave = 'aqui fica sua credencial de conexao com mongodb atlas ou mongodb client'

module.exports.connect = () => {
    mongoose.connect(chave, err => {
        if(err) {
            console.log('==> [-] mongodb');
        } else {
            console.log('==> [+] mongodb');
        }
    }, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        UseCreateIndexes: true
        });
}
