const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate')


const dados = new mongoose.Schema({
    language: {
        type: String,
        required: false,
        trim: true
    },
    tweet: {
        type: String,
        required: false,
        trim: true
    }
});

dados.plugin(mongoosePaginate);

module.exports = mongoose.model('dados', dados);