const express = require('express');
const bp = require('body-parser');
const rota = require('./src/router');
const conexao = require('./src/connection')

const app = express();
var porta = process.env.PORT || 3000

app.use(bp.json());
app.use(bp.urlencoded({extended: false}));
app.use(express.static('public'));
app.use('/', rota);

app.set('view engine', 'ejs');
app.set('views', 'views');





app.listen(porta, (err) => {
    conexao.connect();
    if(err) {
        console.log('==> [-]  falha na aplicação');
    } else {
        console.log('==> [+] aplicação funcionando ');
    }
});