const mongoose = require('mongoose');

const dados = new mongoose.Schema({
    language: {
        type: String,
        required: false,
    },
    tweet: {
        type: String,
        required: false,
    },
}) 

module.exports = mongoose.model('dados', dados)