// const axios = require("axios")
const express = require("express")
const bp = require('body-parser');
const csv = require('csv-parser')
const fs = require('fs')

const conexao = require('./conexao')
require('./model');
const mongoose = require('mongoose');
const modelo =  mongoose.model('dados');


const app = express()
app.use(bp.json())
app.use(bp.urlencoded({extended: false}));



const results = []

fs.createReadStream('./dados.csv')
    .pipe(csv({}))
    .on('data', (data) => {
        results.push(data)

        // aqui salvo as informação no banco
        new modelo(results).save()

    })
    .on('end',  () => {
        console.log(results)
    })





var port = process.env.PORT || 3001

app.listen(port, (err) => {
    conexao.connect()
    if(err) {
        console.log('falha na aplicação');
    } else {
        console.log(`aplicação funcionando [localhost:${port}]`);
    }
});